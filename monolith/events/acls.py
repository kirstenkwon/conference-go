from .keys import PEXELS_API_KEY
import requests

def get_photo(city, state):
    # create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # create the URL for the request with the city and state
    query = f'{city} {state}'
    url = f'https://api.pexels.com/v1/search?query={query}'
    # make the request
    response = requests.get(url, headers=headers)
    # Parse the Json response
    picture_url = response.json()["photos"][0]["src"]["original"]
    # Return a dictionary that contain a picture_url key and
    # one of the URLs for one of the pictures in the response
    return {
        "picture_url": picture_url
    }

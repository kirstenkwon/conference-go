# SETS UP QUEUES

import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# Set the hostname that we'll connect to
parameters = pika.ConnectionParameters(host='rabbitmq')

# Create a connection to RabbitMQ
connection = pika.BlockingConnection(parameters)

# Open a channel to RabbitMQ
channel = connection.channel()

# Create a queue if it does not exist
channel.queue_declare(queue='presentation_approvals')
channel.queue_declare(queue='presentation_rejections')
#^ waiting for queue


#3. SENDS EMAIL
# Create a main method to run
def process_approval(ch, method, properties, body):
    content = json.loads(body)
    name = content["presenter_name"]
    email = content["presenter_email"]
    title = content["title"]
    send_mail(
        "Your presentation has been accepted",
        f"{name}, we're happy to tell you that your presentation {title} has been accepted",
        'admin@conference.go',
        [email],
        fail_silently=False,
    )

#3. SENDS EMAIL
def process_rejection(ch, method, properties, body):
    content = json.loads(body)
    name = content["presenter_name"]
    email = content["presenter_email"]
    title = content["title"]
    send_mail(
        "Your presentation has been rejected",
        f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
        'admin@conference.go',
        [email],
        fail_silently=False,
    )


#2. triggers the queue
channel.basic_consume(
    queue='presentation_approvals',
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.basic_consume(
    queue='presentation_rejections',
    on_message_callback=process_rejection,
    auto_ack=True,
)


# Print a status
print(' [*] Waiting for messages. To exit press CTRL+C')

# Tell RabbitMQ that you're ready to receive messages
channel.start_consuming()


# Just extra stuff to do when the script runs
# if __name__ == '__main__':
#     try:
#         main()
#     except KeyboardInterrupt:
#         print('Interrupted')
#         try:
#             sys.exit(0)
#         except SystemExit:
#             os._exit(0)
